#!/bin/bash

if [ -e /tmp/out/verrou.lock ]
then
  rm /tmp/out/verrou.lock
  exit 22
fi


if [ -e /tmp/intermediaire ]
then
  rm -fr /tmp/intermediaire
  mkdir /tmp/intermediaire
else
  mkdir /tmp/intermediaire
fi


if [ -e /tmp/in ]
then
  var=$(ls -a /tmp/in | sed -e "/\.$/d" | wc -l)
  if [ $var -eq 0 ]
  then
    exit 1
  fi
  cp -r /tmp/in/* /tmp/intermediaire
else
  mkdir /tmp/in
  var=$(ls -a /tmp/in | sed -e "/\.$/d" | wc -l)
  if [ $var -eq 0 ]
  then
    exit 1
  fi
  cp -r /tmp/in/* /tmp/intermediaire
fi


gzip -f /tmp/intermediaire/*



if [ -e /tmp/out ]
then
  cp /tmp/intermediaire/* /tmp/out
  touch /tmp/out/verrou.lock

else
  mkdir /tmp/out
  cp /tmp/intermediaire/* /tmp/out
  touch /tmp/out/verrou.lock

fi

rm /tmp/out/verrou.lock
